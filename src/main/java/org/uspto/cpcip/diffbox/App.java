package org.uspto.cpcip.diffbox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import picocli.CommandLine;

/**
 * Hello world!
 *
 */
public class App 
{
	private static final Logger log = LoggerFactory.getLogger(App.class);
    public static void main( String[] args )
    {
        Diff d =  new Diff();
        new CommandLine(d).parse(args);
        
        log.debug("diff cmd = {}", d);
        
        String output = d.call();
        
    }
}
