package org.uspto.cpcip.diffbox;

import java.io.File;

import picocli.CommandLine.Option;

public class Diff {

	@Option(names = { "-o","--original-file"}, description = "Original file to diff against", 
			required=true)
	private File originalFile;

	@Option(names = { "-n","--new-file"}, description = "Original file to diff against", 
			required=true)
	private File newFile;

	@Option(names = { "-t","--temp-dir"}, description = "Directory used as parent to dynamic generated work dir", 
			required=true)
	private File tmpDir;

	@Option(names = { "-d","--dry-run"}, description = "Check files. log steps", 
			required=false)
	private boolean doDryRunOnly;

	@Override
	public String toString() {
		return "Diff [originalFile=" + originalFile + ", newFile=" + newFile + ", tmpDir=" + tmpDir + ", doDryRunOnly="
				+ doDryRunOnly + "]";
	}

	public String call() {
		StringBuilder sb= new StringBuilder();
		return sb.toString();
	}

	
	
	
	

	
	
}
